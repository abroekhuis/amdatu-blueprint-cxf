package net.luminis.resource;

import javax.jws.WebService;

@WebService
public interface Resource {

    String sayHello();

}
