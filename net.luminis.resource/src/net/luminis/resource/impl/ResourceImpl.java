package net.luminis.resource.impl;

import javax.jws.WebService;

import net.luminis.resource.Resource;

@WebService(name="test")
public class ResourceImpl implements Resource {

    public String sayHello() {
        return "Hello soapy world!";
    }

}