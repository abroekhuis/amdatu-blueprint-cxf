package net.luminis.uwlr.leerresultaten.resource;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import nl.edustandaard.leerresultaten._2.leerresultaten.PrtLeerresultaten;
import nl.edustandaard.leerresultaten._2.leerresultaten.PrtLeerresultatenServiceImpl;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent()
				.setInterface(PrtLeerresultaten.class.getName(), null)
				.setImplementation(PrtLeerresultatenServiceImpl.class));
	}

}
