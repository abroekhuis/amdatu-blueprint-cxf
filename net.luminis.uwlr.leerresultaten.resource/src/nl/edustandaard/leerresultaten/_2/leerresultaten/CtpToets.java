
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ctpToets complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpToets"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="toetscode" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld"/&gt;
 *         &lt;element name="versie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld" minOccurs="0"/&gt;
 *         &lt;element name="toetsnaam" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString128" minOccurs="0"/&gt;
 *         &lt;element name="leerjaar" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld" minOccurs="0"/&gt;
 *         &lt;element name="vakgebied" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld" minOccurs="0"/&gt;
 *         &lt;element name="toetsnormering" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpNormering" minOccurs="0"/&gt;
 *         &lt;element name="toetshierarchie" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ingang" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.edustandaard.nl/leerresultaten/2/leerresultaten&gt;ctpVocabulaireGebondenVeld"&gt;
 *                           &lt;attribute name="niveau" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="toetsonderdelen" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="toetsonderdeel" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpToetsOnderdeel" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpToets", propOrder = {
    "toetscode",
    "versie",
    "toetsnaam",
    "leerjaar",
    "vakgebied",
    "toetsnormering",
    "toetshierarchie",
    "toetsonderdelen"
})
public class CtpToets {

    @XmlElement(required = true)
    protected CtpVocabulaireGebondenVeld toetscode;
    protected CtpVocabulaireGebondenVeld versie;
    protected String toetsnaam;
    protected CtpVocabulaireGebondenVeld leerjaar;
    protected CtpVocabulaireGebondenVeld vakgebied;
    protected CtpNormering toetsnormering;
    protected CtpToets.Toetshierarchie toetshierarchie;
    protected CtpToets.Toetsonderdelen toetsonderdelen;

    /**
     * Gets the value of the toetscode property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getToetscode() {
        return toetscode;
    }

    /**
     * Sets the value of the toetscode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setToetscode(CtpVocabulaireGebondenVeld value) {
        this.toetscode = value;
    }

    /**
     * Gets the value of the versie property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getVersie() {
        return versie;
    }

    /**
     * Sets the value of the versie property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setVersie(CtpVocabulaireGebondenVeld value) {
        this.versie = value;
    }

    /**
     * Gets the value of the toetsnaam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToetsnaam() {
        return toetsnaam;
    }

    /**
     * Sets the value of the toetsnaam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToetsnaam(String value) {
        this.toetsnaam = value;
    }

    /**
     * Gets the value of the leerjaar property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getLeerjaar() {
        return leerjaar;
    }

    /**
     * Sets the value of the leerjaar property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setLeerjaar(CtpVocabulaireGebondenVeld value) {
        this.leerjaar = value;
    }

    /**
     * Gets the value of the vakgebied property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getVakgebied() {
        return vakgebied;
    }

    /**
     * Sets the value of the vakgebied property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setVakgebied(CtpVocabulaireGebondenVeld value) {
        this.vakgebied = value;
    }

    /**
     * Gets the value of the toetsnormering property.
     * 
     * @return
     *     possible object is
     *     {@link CtpNormering }
     *     
     */
    public CtpNormering getToetsnormering() {
        return toetsnormering;
    }

    /**
     * Sets the value of the toetsnormering property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpNormering }
     *     
     */
    public void setToetsnormering(CtpNormering value) {
        this.toetsnormering = value;
    }

    /**
     * Gets the value of the toetshierarchie property.
     * 
     * @return
     *     possible object is
     *     {@link CtpToets.Toetshierarchie }
     *     
     */
    public CtpToets.Toetshierarchie getToetshierarchie() {
        return toetshierarchie;
    }

    /**
     * Sets the value of the toetshierarchie property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpToets.Toetshierarchie }
     *     
     */
    public void setToetshierarchie(CtpToets.Toetshierarchie value) {
        this.toetshierarchie = value;
    }

    /**
     * Gets the value of the toetsonderdelen property.
     * 
     * @return
     *     possible object is
     *     {@link CtpToets.Toetsonderdelen }
     *     
     */
    public CtpToets.Toetsonderdelen getToetsonderdelen() {
        return toetsonderdelen;
    }

    /**
     * Sets the value of the toetsonderdelen property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpToets.Toetsonderdelen }
     *     
     */
    public void setToetsonderdelen(CtpToets.Toetsonderdelen value) {
        this.toetsonderdelen = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ingang" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.edustandaard.nl/leerresultaten/2/leerresultaten&gt;ctpVocabulaireGebondenVeld"&gt;
     *                 &lt;attribute name="niveau" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ingang"
    })
    public static class Toetshierarchie {

        @XmlElement(required = true)
        protected List<CtpToets.Toetshierarchie.Ingang> ingang;

        /**
         * Gets the value of the ingang property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ingang property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getIngang().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CtpToets.Toetshierarchie.Ingang }
         * 
         * 
         */
        public List<CtpToets.Toetshierarchie.Ingang> getIngang() {
            if (ingang == null) {
                ingang = new ArrayList<CtpToets.Toetshierarchie.Ingang>();
            }
            return this.ingang;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.edustandaard.nl/leerresultaten/2/leerresultaten&gt;ctpVocabulaireGebondenVeld"&gt;
         *       &lt;attribute name="niveau" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Ingang
            extends CtpVocabulaireGebondenVeld
        {

            @XmlAttribute(name = "niveau", required = true)
            protected BigInteger niveau;

            /**
             * Gets the value of the niveau property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNiveau() {
                return niveau;
            }

            /**
             * Sets the value of the niveau property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNiveau(BigInteger value) {
                this.niveau = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="toetsonderdeel" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpToetsOnderdeel" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "toetsonderdeel"
    })
    public static class Toetsonderdelen {

        @XmlElement(required = true)
        protected List<CtpToetsOnderdeel> toetsonderdeel;

        /**
         * Gets the value of the toetsonderdeel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the toetsonderdeel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getToetsonderdeel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CtpToetsOnderdeel }
         * 
         * 
         */
        public List<CtpToetsOnderdeel> getToetsonderdeel() {
            if (toetsonderdeel == null) {
                toetsonderdeel = new ArrayList<CtpToetsOnderdeel>();
            }
            return this.toetsonderdeel;
        }

    }

}
