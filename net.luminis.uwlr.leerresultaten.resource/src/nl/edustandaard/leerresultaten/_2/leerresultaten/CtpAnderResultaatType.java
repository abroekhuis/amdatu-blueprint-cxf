
package nl.edustandaard.leerresultaten._2.leerresultaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Structuur om het formaat van eigen resultaat in te definieren. Het veld andere_score is voorbeeldmatig toegevoegd.
 * 
 * <p>Java class for ctpAnderResultaatType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpAnderResultaatType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="andere_score" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpAnderResultaatType", propOrder = {
    "andereScore"
})
public abstract class CtpAnderResultaatType {

    @XmlElement(name = "andere_score")
    protected String andereScore;

    /**
     * Gets the value of the andereScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAndereScore() {
        return andereScore;
    }

    /**
     * Sets the value of the andereScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAndereScore(String value) {
        this.andereScore = value;
    }

}
