
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import nl.edustandaard.oso_gegevensset._1_2.dossier.ReferentiescoreType;
import nl.edustandaard.oso_gegevensset._1_2.dossier.ToetsscoreType;


/**
 * Resultaat volgens de OSO definities
 * 
 * <p>Java class for ctpOsoResultaat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpOsoResultaat"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}toetsscore" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}referentiescore" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpOsoResultaat", propOrder = {
    "toetsscore",
    "referentiescore"
})
public class CtpOsoResultaat {

    @XmlElement(namespace = "http://www.edustandaard.nl/oso_gegevensset/1.2/dossier")
    protected ToetsscoreType toetsscore;
    @XmlElement(namespace = "http://www.edustandaard.nl/oso_gegevensset/1.2/dossier")
    protected List<ReferentiescoreType> referentiescore;

    /**
     * Gets the value of the toetsscore property.
     * 
     * @return
     *     possible object is
     *     {@link ToetsscoreType }
     *     
     */
    public ToetsscoreType getToetsscore() {
        return toetsscore;
    }

    /**
     * Sets the value of the toetsscore property.
     * 
     * @param value
     *     allowed object is
     *     {@link ToetsscoreType }
     *     
     */
    public void setToetsscore(ToetsscoreType value) {
        this.toetsscore = value;
    }

    /**
     * Gets the value of the referentiescore property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referentiescore property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferentiescore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferentiescoreType }
     * 
     * 
     */
    public List<ReferentiescoreType> getReferentiescore() {
        if (referentiescore == null) {
            referentiescore = new ArrayList<ReferentiescoreType>();
        }
        return this.referentiescore;
    }

}
