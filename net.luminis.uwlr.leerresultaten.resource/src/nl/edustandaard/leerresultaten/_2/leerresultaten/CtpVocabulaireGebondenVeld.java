
package nl.edustandaard.leerresultaten._2.leerresultaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for ctpVocabulaireGebondenVeld complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpVocabulaireGebondenVeld"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.edustandaard.nl/leerresultaten/2/leerresultaten&gt;tpString256"&gt;
 *       &lt;attGroup ref="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}agVocabulaireBinding"/&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpVocabulaireGebondenVeld", propOrder = {
    "value"
})
@XmlSeeAlso({
    nl.edustandaard.leerresultaten._2.leerresultaten.CtpToets.Toetshierarchie.Ingang.class
})
public class CtpVocabulaireGebondenVeld {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "vocabulaire")
    @XmlSchemaType(name = "anyURI")
    protected String vocabulaire;
    @XmlAttribute(name = "vocabulairelocatie")
    @XmlSchemaType(name = "anyURI")
    protected String vocabulairelocatie;

    /**
     * String met minimale lengte 1 en maximale lengte 256.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the vocabulaire property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVocabulaire() {
        return vocabulaire;
    }

    /**
     * Sets the value of the vocabulaire property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVocabulaire(String value) {
        this.vocabulaire = value;
    }

    /**
     * Gets the value of the vocabulairelocatie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVocabulairelocatie() {
        return vocabulairelocatie;
    }

    /**
     * Sets the value of the vocabulairelocatie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVocabulairelocatie(String value) {
        this.vocabulairelocatie = value;
    }

}
