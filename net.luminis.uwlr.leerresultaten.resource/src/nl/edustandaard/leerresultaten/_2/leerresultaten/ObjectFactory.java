
package nl.edustandaard.leerresultaten._2.leerresultaten;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.edustandaard.leerresultaten._2.leerresultaten package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LeerresultatenBevestiging_QNAME = new QName("http://www.edustandaard.nl/leerresultaten/2/leerresultaten", "leerresultaten_bevestiging");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.edustandaard.leerresultaten._2.leerresultaten
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LeerresultatenVerzoek }
     * 
     */
    public LeerresultatenVerzoek createLeerresultatenVerzoek() {
        return new LeerresultatenVerzoek();
    }

    /**
     * Create an instance of {@link CtpNormering }
     * 
     */
    public CtpNormering createCtpNormering() {
        return new CtpNormering();
    }

    /**
     * Create an instance of {@link CtpNormering.Norm }
     * 
     */
    public CtpNormering.Norm createCtpNormeringNorm() {
        return new CtpNormering.Norm();
    }

    /**
     * Create an instance of {@link CtpToets }
     * 
     */
    public CtpToets createCtpToets() {
        return new CtpToets();
    }

    /**
     * Create an instance of {@link CtpToets.Toetshierarchie }
     * 
     */
    public CtpToets.Toetshierarchie createCtpToetsToetshierarchie() {
        return new CtpToets.Toetshierarchie();
    }

    /**
     * Create an instance of {@link CtpToetsafname }
     * 
     */
    public CtpToetsafname createCtpToetsafname() {
        return new CtpToetsafname();
    }

    /**
     * Create an instance of {@link CtpSchool }
     * 
     */
    public CtpSchool createCtpSchool() {
        return new CtpSchool();
    }

    /**
     * Create an instance of {@link LeerresultatenVerzoek.Toetsafnames }
     * 
     */
    public LeerresultatenVerzoek.Toetsafnames createLeerresultatenVerzoekToetsafnames() {
        return new LeerresultatenVerzoek.Toetsafnames();
    }

    /**
     * Create an instance of {@link LeerresultatenVerzoek.Toetsen }
     * 
     */
    public LeerresultatenVerzoek.Toetsen createLeerresultatenVerzoekToetsen() {
        return new LeerresultatenVerzoek.Toetsen();
    }

    /**
     * Create an instance of {@link CtpLeeg }
     * 
     */
    public CtpLeeg createCtpLeeg() {
        return new CtpLeeg();
    }

    /**
     * Create an instance of {@link CtpResultaat }
     * 
     */
    public CtpResultaat createCtpResultaat() {
        return new CtpResultaat();
    }

    /**
     * Create an instance of {@link CtpToetsOnderdeel }
     * 
     */
    public CtpToetsOnderdeel createCtpToetsOnderdeel() {
        return new CtpToetsOnderdeel();
    }

    /**
     * Create an instance of {@link CtpOsoResultaat }
     * 
     */
    public CtpOsoResultaat createCtpOsoResultaat() {
        return new CtpOsoResultaat();
    }

    /**
     * Create an instance of {@link CtpVocabulaireGebondenVeld }
     * 
     */
    public CtpVocabulaireGebondenVeld createCtpVocabulaireGebondenVeld() {
        return new CtpVocabulaireGebondenVeld();
    }

    /**
     * Create an instance of {@link CtpNormering.Norm.Normkleur }
     * 
     */
    public CtpNormering.Norm.Normkleur createCtpNormeringNormNormkleur() {
        return new CtpNormering.Norm.Normkleur();
    }

    /**
     * Create an instance of {@link CtpToets.Toetsonderdelen }
     * 
     */
    public CtpToets.Toetsonderdelen createCtpToetsToetsonderdelen() {
        return new CtpToets.Toetsonderdelen();
    }

    /**
     * Create an instance of {@link CtpToets.Toetshierarchie.Ingang }
     * 
     */
    public CtpToets.Toetshierarchie.Ingang createCtpToetsToetshierarchieIngang() {
        return new CtpToets.Toetshierarchie.Ingang();
    }

    /**
     * Create an instance of {@link CtpToetsafname.Resultaten }
     * 
     */
    public CtpToetsafname.Resultaten createCtpToetsafnameResultaten() {
        return new CtpToetsafname.Resultaten();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CtpLeeg }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.edustandaard.nl/leerresultaten/2/leerresultaten", name = "leerresultaten_bevestiging")
    public JAXBElement<CtpLeeg> createLeerresultatenBevestiging(CtpLeeg value) {
        return new JAXBElement<CtpLeeg>(_LeerresultatenBevestiging_QNAME, CtpLeeg.class, null, value);
    }

}
