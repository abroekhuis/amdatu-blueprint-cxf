
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ctpResultaat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpResultaat"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="afnamedatum" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="toetscode" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKey"/&gt;
 *         &lt;element name="versie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld" minOccurs="0"/&gt;
 *         &lt;element name="toetsonderdeelcode" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKey" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="score" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpScore"/&gt;
 *           &lt;element name="osoresultaat" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpOsoResultaat"/&gt;
 *           &lt;element name="anderresultaat" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpAnderResultaatType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="infourl" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString256" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="key" use="required" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKey" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpResultaat", propOrder = {
    "afnamedatum",
    "toetscode",
    "versie",
    "toetsonderdeelcode",
    "score",
    "osoresultaat",
    "anderresultaat",
    "infourl"
})
public class CtpResultaat {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar afnamedatum;
    @XmlElement(required = true)
    protected String toetscode;
    protected CtpVocabulaireGebondenVeld versie;
    protected String toetsonderdeelcode;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger score;
    protected CtpOsoResultaat osoresultaat;
    protected CtpAnderResultaatType anderresultaat;
    protected String infourl;
    @XmlAttribute(name = "key", required = true)
    protected String key;

    /**
     * Gets the value of the afnamedatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAfnamedatum() {
        return afnamedatum;
    }

    /**
     * Sets the value of the afnamedatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAfnamedatum(XMLGregorianCalendar value) {
        this.afnamedatum = value;
    }

    /**
     * Gets the value of the toetscode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToetscode() {
        return toetscode;
    }

    /**
     * Sets the value of the toetscode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToetscode(String value) {
        this.toetscode = value;
    }

    /**
     * Gets the value of the versie property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getVersie() {
        return versie;
    }

    /**
     * Sets the value of the versie property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setVersie(CtpVocabulaireGebondenVeld value) {
        this.versie = value;
    }

    /**
     * Gets the value of the toetsonderdeelcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToetsonderdeelcode() {
        return toetsonderdeelcode;
    }

    /**
     * Sets the value of the toetsonderdeelcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToetsonderdeelcode(String value) {
        this.toetsonderdeelcode = value;
    }

    /**
     * Gets the value of the score property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setScore(BigInteger value) {
        this.score = value;
    }

    /**
     * Gets the value of the osoresultaat property.
     * 
     * @return
     *     possible object is
     *     {@link CtpOsoResultaat }
     *     
     */
    public CtpOsoResultaat getOsoresultaat() {
        return osoresultaat;
    }

    /**
     * Sets the value of the osoresultaat property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpOsoResultaat }
     *     
     */
    public void setOsoresultaat(CtpOsoResultaat value) {
        this.osoresultaat = value;
    }

    /**
     * Gets the value of the anderresultaat property.
     * 
     * @return
     *     possible object is
     *     {@link CtpAnderResultaatType }
     *     
     */
    public CtpAnderResultaatType getAnderresultaat() {
        return anderresultaat;
    }

    /**
     * Sets the value of the anderresultaat property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpAnderResultaatType }
     *     
     */
    public void setAnderresultaat(CtpAnderResultaatType value) {
        this.anderresultaat = value;
    }

    /**
     * Gets the value of the infourl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfourl() {
        return infourl;
    }

    /**
     * Sets the value of the infourl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfourl(String value) {
        this.infourl = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

}
