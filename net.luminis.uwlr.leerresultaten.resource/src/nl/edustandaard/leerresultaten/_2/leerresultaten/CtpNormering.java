
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Alleen relevant als het resultaat als een score wordt weergegeven. In het geval van een OSO resultaat moet dit indien aanwezig genegeerd worden.
 * 
 * <p>Java class for ctpNormering complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpNormering"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="toetscategorie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld" minOccurs="0"/&gt;
 *         &lt;element name="toetsniveau" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld" minOccurs="0"/&gt;
 *         &lt;element name="wegingsfactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="norm" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="term" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString64"/&gt;
 *                   &lt;element name="omschrijving" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString128" minOccurs="0"/&gt;
 *                   &lt;element name="beginnormwaarde" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpScore"/&gt;
 *                   &lt;element name="eindnormwaarde" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpScore"/&gt;
 *                   &lt;element name="normkleur" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="rood_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
 *                             &lt;element name="groen_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
 *                             &lt;element name="blauw_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
 *                             &lt;element name="transparantie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpTransparatiecode"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="schoolcijfer_vanaf" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpSchoolcijfer" minOccurs="0"/&gt;
 *                   &lt;element name="schoolcijfer_totenmet" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpSchoolcijfer" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}agVocabulaireBinding"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpNormering", propOrder = {
    "toetscategorie",
    "toetsniveau",
    "wegingsfactor",
    "norm"
})
public class CtpNormering {

    protected CtpVocabulaireGebondenVeld toetscategorie;
    protected CtpVocabulaireGebondenVeld toetsniveau;
    protected BigDecimal wegingsfactor;
    @XmlElement(required = true)
    protected List<CtpNormering.Norm> norm;
    @XmlAttribute(name = "vocabulaire")
    @XmlSchemaType(name = "anyURI")
    protected String vocabulaire;
    @XmlAttribute(name = "vocabulairelocatie")
    @XmlSchemaType(name = "anyURI")
    protected String vocabulairelocatie;

    /**
     * Gets the value of the toetscategorie property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getToetscategorie() {
        return toetscategorie;
    }

    /**
     * Sets the value of the toetscategorie property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setToetscategorie(CtpVocabulaireGebondenVeld value) {
        this.toetscategorie = value;
    }

    /**
     * Gets the value of the toetsniveau property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getToetsniveau() {
        return toetsniveau;
    }

    /**
     * Sets the value of the toetsniveau property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setToetsniveau(CtpVocabulaireGebondenVeld value) {
        this.toetsniveau = value;
    }

    /**
     * Gets the value of the wegingsfactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWegingsfactor() {
        return wegingsfactor;
    }

    /**
     * Sets the value of the wegingsfactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWegingsfactor(BigDecimal value) {
        this.wegingsfactor = value;
    }

    /**
     * Gets the value of the norm property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the norm property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNorm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CtpNormering.Norm }
     * 
     * 
     */
    public List<CtpNormering.Norm> getNorm() {
        if (norm == null) {
            norm = new ArrayList<CtpNormering.Norm>();
        }
        return this.norm;
    }

    /**
     * Gets the value of the vocabulaire property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVocabulaire() {
        return vocabulaire;
    }

    /**
     * Sets the value of the vocabulaire property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVocabulaire(String value) {
        this.vocabulaire = value;
    }

    /**
     * Gets the value of the vocabulairelocatie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVocabulairelocatie() {
        return vocabulairelocatie;
    }

    /**
     * Sets the value of the vocabulairelocatie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVocabulairelocatie(String value) {
        this.vocabulairelocatie = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="term" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString64"/&gt;
     *         &lt;element name="omschrijving" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString128" minOccurs="0"/&gt;
     *         &lt;element name="beginnormwaarde" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpScore"/&gt;
     *         &lt;element name="eindnormwaarde" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpScore"/&gt;
     *         &lt;element name="normkleur" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="rood_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
     *                   &lt;element name="groen_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
     *                   &lt;element name="blauw_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
     *                   &lt;element name="transparantie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpTransparatiecode"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="schoolcijfer_vanaf" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpSchoolcijfer" minOccurs="0"/&gt;
     *         &lt;element name="schoolcijfer_totenmet" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpSchoolcijfer" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "term",
        "omschrijving",
        "beginnormwaarde",
        "eindnormwaarde",
        "normkleur",
        "schoolcijferVanaf",
        "schoolcijferTotenmet"
    })
    public static class Norm {

        @XmlElement(required = true)
        protected String term;
        protected String omschrijving;
        @XmlElement(required = true)
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger beginnormwaarde;
        @XmlElement(required = true)
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger eindnormwaarde;
        protected CtpNormering.Norm.Normkleur normkleur;
        @XmlElement(name = "schoolcijfer_vanaf")
        protected BigDecimal schoolcijferVanaf;
        @XmlElement(name = "schoolcijfer_totenmet")
        protected BigDecimal schoolcijferTotenmet;

        /**
         * Gets the value of the term property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTerm() {
            return term;
        }

        /**
         * Sets the value of the term property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTerm(String value) {
            this.term = value;
        }

        /**
         * Gets the value of the omschrijving property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOmschrijving() {
            return omschrijving;
        }

        /**
         * Sets the value of the omschrijving property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOmschrijving(String value) {
            this.omschrijving = value;
        }

        /**
         * Gets the value of the beginnormwaarde property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBeginnormwaarde() {
            return beginnormwaarde;
        }

        /**
         * Sets the value of the beginnormwaarde property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBeginnormwaarde(BigInteger value) {
            this.beginnormwaarde = value;
        }

        /**
         * Gets the value of the eindnormwaarde property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEindnormwaarde() {
            return eindnormwaarde;
        }

        /**
         * Sets the value of the eindnormwaarde property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEindnormwaarde(BigInteger value) {
            this.eindnormwaarde = value;
        }

        /**
         * Gets the value of the normkleur property.
         * 
         * @return
         *     possible object is
         *     {@link CtpNormering.Norm.Normkleur }
         *     
         */
        public CtpNormering.Norm.Normkleur getNormkleur() {
            return normkleur;
        }

        /**
         * Sets the value of the normkleur property.
         * 
         * @param value
         *     allowed object is
         *     {@link CtpNormering.Norm.Normkleur }
         *     
         */
        public void setNormkleur(CtpNormering.Norm.Normkleur value) {
            this.normkleur = value;
        }

        /**
         * Gets the value of the schoolcijferVanaf property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSchoolcijferVanaf() {
            return schoolcijferVanaf;
        }

        /**
         * Sets the value of the schoolcijferVanaf property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSchoolcijferVanaf(BigDecimal value) {
            this.schoolcijferVanaf = value;
        }

        /**
         * Gets the value of the schoolcijferTotenmet property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSchoolcijferTotenmet() {
            return schoolcijferTotenmet;
        }

        /**
         * Sets the value of the schoolcijferTotenmet property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSchoolcijferTotenmet(BigDecimal value) {
            this.schoolcijferTotenmet = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="rood_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
         *         &lt;element name="groen_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
         *         &lt;element name="blauw_code" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKleurcode"/&gt;
         *         &lt;element name="transparantie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpTransparatiecode"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roodCode",
            "groenCode",
            "blauwCode",
            "transparantie"
        })
        public static class Normkleur {

            @XmlElement(name = "rood_code")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected int roodCode;
            @XmlElement(name = "groen_code")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected int groenCode;
            @XmlElement(name = "blauw_code")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected int blauwCode;
            @XmlElement(required = true)
            protected BigDecimal transparantie;

            /**
             * Gets the value of the roodCode property.
             * 
             */
            public int getRoodCode() {
                return roodCode;
            }

            /**
             * Sets the value of the roodCode property.
             * 
             */
            public void setRoodCode(int value) {
                this.roodCode = value;
            }

            /**
             * Gets the value of the groenCode property.
             * 
             */
            public int getGroenCode() {
                return groenCode;
            }

            /**
             * Sets the value of the groenCode property.
             * 
             */
            public void setGroenCode(int value) {
                this.groenCode = value;
            }

            /**
             * Gets the value of the blauwCode property.
             * 
             */
            public int getBlauwCode() {
                return blauwCode;
            }

            /**
             * Sets the value of the blauwCode property.
             * 
             */
            public void setBlauwCode(int value) {
                this.blauwCode = value;
            }

            /**
             * Gets the value of the transparantie property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTransparantie() {
                return transparantie;
            }

            /**
             * Sets the value of the transparantie property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTransparantie(BigDecimal value) {
                this.transparantie = value;
            }

        }

    }

}
