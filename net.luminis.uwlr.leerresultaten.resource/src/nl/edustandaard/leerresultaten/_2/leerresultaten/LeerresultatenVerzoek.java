
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="school" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpSchool"/&gt;
 *         &lt;element name="toetsafnames"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="toetsafname" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpToetsafname" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;group ref="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}gToetsen"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "school",
    "toetsafnames",
    "toetsen"
})
@XmlRootElement(name = "leerresultaten_verzoek")
public class LeerresultatenVerzoek {

    @XmlElement(required = true)
    protected CtpSchool school;
    @XmlElement(required = true)
    protected LeerresultatenVerzoek.Toetsafnames toetsafnames;
    @XmlElement(required = true)
    protected LeerresultatenVerzoek.Toetsen toetsen;

    /**
     * Gets the value of the school property.
     * 
     * @return
     *     possible object is
     *     {@link CtpSchool }
     *     
     */
    public CtpSchool getSchool() {
        return school;
    }

    /**
     * Sets the value of the school property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpSchool }
     *     
     */
    public void setSchool(CtpSchool value) {
        this.school = value;
    }

    /**
     * Gets the value of the toetsafnames property.
     * 
     * @return
     *     possible object is
     *     {@link LeerresultatenVerzoek.Toetsafnames }
     *     
     */
    public LeerresultatenVerzoek.Toetsafnames getToetsafnames() {
        return toetsafnames;
    }

    /**
     * Sets the value of the toetsafnames property.
     * 
     * @param value
     *     allowed object is
     *     {@link LeerresultatenVerzoek.Toetsafnames }
     *     
     */
    public void setToetsafnames(LeerresultatenVerzoek.Toetsafnames value) {
        this.toetsafnames = value;
    }

    /**
     * Gets the value of the toetsen property.
     * 
     * @return
     *     possible object is
     *     {@link LeerresultatenVerzoek.Toetsen }
     *     
     */
    public LeerresultatenVerzoek.Toetsen getToetsen() {
        return toetsen;
    }

    /**
     * Sets the value of the toetsen property.
     * 
     * @param value
     *     allowed object is
     *     {@link LeerresultatenVerzoek.Toetsen }
     *     
     */
    public void setToetsen(LeerresultatenVerzoek.Toetsen value) {
        this.toetsen = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="toetsafname" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpToetsafname" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "toetsafname"
    })
    public static class Toetsafnames {

        @XmlElement(required = true)
        protected List<CtpToetsafname> toetsafname;

        /**
         * Gets the value of the toetsafname property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the toetsafname property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getToetsafname().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CtpToetsafname }
         * 
         * 
         */
        public List<CtpToetsafname> getToetsafname() {
            if (toetsafname == null) {
                toetsafname = new ArrayList<CtpToetsafname>();
            }
            return this.toetsafname;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="toets" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpToets" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "toets"
    })
    public static class Toetsen {

        @XmlElement(required = true)
        protected List<CtpToets> toets;

        /**
         * Gets the value of the toets property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the toets property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getToets().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CtpToets }
         * 
         * 
         */
        public List<CtpToets> getToets() {
            if (toets == null) {
                toets = new ArrayList<CtpToets>();
            }
            return this.toets;
        }

    }

}
