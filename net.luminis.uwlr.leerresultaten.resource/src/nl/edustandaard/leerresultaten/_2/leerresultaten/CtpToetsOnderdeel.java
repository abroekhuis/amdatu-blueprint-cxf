
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ctpToetsOnderdeel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpToetsOnderdeel"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="toetsonderdeelvolgnummer" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *         &lt;element name="toetsonderdeelcode" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpVocabulaireGebondenVeld"/&gt;
 *         &lt;element name="toetsonderdeelnaam" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString128" minOccurs="0"/&gt;
 *         &lt;element name="toetsonderdeelnormering" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpNormering" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpToetsOnderdeel", propOrder = {
    "toetsonderdeelvolgnummer",
    "toetsonderdeelcode",
    "toetsonderdeelnaam",
    "toetsonderdeelnormering"
})
public class CtpToetsOnderdeel {

    @XmlElement(required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger toetsonderdeelvolgnummer;
    @XmlElement(required = true)
    protected CtpVocabulaireGebondenVeld toetsonderdeelcode;
    protected String toetsonderdeelnaam;
    protected CtpNormering toetsonderdeelnormering;

    /**
     * Gets the value of the toetsonderdeelvolgnummer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getToetsonderdeelvolgnummer() {
        return toetsonderdeelvolgnummer;
    }

    /**
     * Sets the value of the toetsonderdeelvolgnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setToetsonderdeelvolgnummer(BigInteger value) {
        this.toetsonderdeelvolgnummer = value;
    }

    /**
     * Gets the value of the toetsonderdeelcode property.
     * 
     * @return
     *     possible object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public CtpVocabulaireGebondenVeld getToetsonderdeelcode() {
        return toetsonderdeelcode;
    }

    /**
     * Sets the value of the toetsonderdeelcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpVocabulaireGebondenVeld }
     *     
     */
    public void setToetsonderdeelcode(CtpVocabulaireGebondenVeld value) {
        this.toetsonderdeelcode = value;
    }

    /**
     * Gets the value of the toetsonderdeelnaam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToetsonderdeelnaam() {
        return toetsonderdeelnaam;
    }

    /**
     * Sets the value of the toetsonderdeelnaam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToetsonderdeelnaam(String value) {
        this.toetsonderdeelnaam = value;
    }

    /**
     * Gets the value of the toetsonderdeelnormering property.
     * 
     * @return
     *     possible object is
     *     {@link CtpNormering }
     *     
     */
    public CtpNormering getToetsonderdeelnormering() {
        return toetsonderdeelnormering;
    }

    /**
     * Sets the value of the toetsonderdeelnormering property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpNormering }
     *     
     */
    public void setToetsonderdeelnormering(CtpNormering value) {
        this.toetsonderdeelnormering = value;
    }

}
