
package nl.edustandaard.leerresultaten._2.leerresultaten;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ctpToetsafname complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpToetsafname"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="leerlingid" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKey"/&gt;
 *         &lt;element name="eckid" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKey" minOccurs="0"/&gt;
 *         &lt;element name="resultaatverwerkerid" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpKey" minOccurs="0"/&gt;
 *         &lt;element name="resultaten"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="resultaat" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpResultaat" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpToetsafname", propOrder = {
    "leerlingid",
    "eckid",
    "resultaatverwerkerid",
    "resultaten"
})
public class CtpToetsafname {

    @XmlElement(required = true)
    protected String leerlingid;
    protected String eckid;
    protected String resultaatverwerkerid;
    @XmlElement(required = true)
    protected CtpToetsafname.Resultaten resultaten;

    /**
     * Gets the value of the leerlingid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeerlingid() {
        return leerlingid;
    }

    /**
     * Sets the value of the leerlingid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeerlingid(String value) {
        this.leerlingid = value;
    }

    /**
     * Gets the value of the eckid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEckid() {
        return eckid;
    }

    /**
     * Sets the value of the eckid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEckid(String value) {
        this.eckid = value;
    }

    /**
     * Gets the value of the resultaatverwerkerid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultaatverwerkerid() {
        return resultaatverwerkerid;
    }

    /**
     * Sets the value of the resultaatverwerkerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultaatverwerkerid(String value) {
        this.resultaatverwerkerid = value;
    }

    /**
     * Gets the value of the resultaten property.
     * 
     * @return
     *     possible object is
     *     {@link CtpToetsafname.Resultaten }
     *     
     */
    public CtpToetsafname.Resultaten getResultaten() {
        return resultaten;
    }

    /**
     * Sets the value of the resultaten property.
     * 
     * @param value
     *     allowed object is
     *     {@link CtpToetsafname.Resultaten }
     *     
     */
    public void setResultaten(CtpToetsafname.Resultaten value) {
        this.resultaten = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="resultaat" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}ctpResultaat" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultaat"
    })
    public static class Resultaten {

        @XmlElement(required = true)
        protected List<CtpResultaat> resultaat;

        /**
         * Gets the value of the resultaat property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the resultaat property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResultaat().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CtpResultaat }
         * 
         * 
         */
        public List<CtpResultaat> getResultaat() {
            if (resultaat == null) {
                resultaat = new ArrayList<CtpResultaat>();
            }
            return this.resultaat;
        }

    }

}
