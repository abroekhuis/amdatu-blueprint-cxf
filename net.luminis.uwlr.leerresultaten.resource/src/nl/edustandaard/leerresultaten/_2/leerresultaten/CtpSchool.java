
package nl.edustandaard.leerresultaten._2.leerresultaten;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ctpSchool complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ctpSchool"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="schooljaar" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpSchooljaar"/&gt;
 *         &lt;group ref="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}gSchoolIdentificatie"/&gt;
 *         &lt;element name="aanmaakdatum" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="auteur" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString128" minOccurs="0"/&gt;
 *         &lt;element name="xsdversie" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString32"/&gt;
 *         &lt;element name="commentaar" type="{http://www.edustandaard.nl/leerresultaten/2/leerresultaten}tpString128" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ctpSchool", propOrder = {
    "schooljaar",
    "brincode",
    "dependancecode",
    "schoolkey",
    "aanmaakdatum",
    "auteur",
    "xsdversie",
    "commentaar"
})
public class CtpSchool {

    @XmlElement(required = true)
    protected String schooljaar;
    protected String brincode;
    @XmlElement(defaultValue = "00")
    protected String dependancecode;
    protected String schoolkey;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aanmaakdatum;
    protected String auteur;
    @XmlElement(required = true)
    protected String xsdversie;
    protected String commentaar;

    /**
     * Gets the value of the schooljaar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchooljaar() {
        return schooljaar;
    }

    /**
     * Sets the value of the schooljaar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchooljaar(String value) {
        this.schooljaar = value;
    }

    /**
     * Gets the value of the brincode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrincode() {
        return brincode;
    }

    /**
     * Sets the value of the brincode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrincode(String value) {
        this.brincode = value;
    }

    /**
     * Gets the value of the dependancecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependancecode() {
        return dependancecode;
    }

    /**
     * Sets the value of the dependancecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependancecode(String value) {
        this.dependancecode = value;
    }

    /**
     * Gets the value of the schoolkey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchoolkey() {
        return schoolkey;
    }

    /**
     * Sets the value of the schoolkey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchoolkey(String value) {
        this.schoolkey = value;
    }

    /**
     * Gets the value of the aanmaakdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAanmaakdatum() {
        return aanmaakdatum;
    }

    /**
     * Sets the value of the aanmaakdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAanmaakdatum(XMLGregorianCalendar value) {
        this.aanmaakdatum = value;
    }

    /**
     * Gets the value of the auteur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * Sets the value of the auteur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuteur(String value) {
        this.auteur = value;
    }

    /**
     * Gets the value of the xsdversie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXsdversie() {
        return xsdversie;
    }

    /**
     * Sets the value of the xsdversie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXsdversie(String value) {
        this.xsdversie = value;
    }

    /**
     * Gets the value of the commentaar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentaar() {
        return commentaar;
    }

    /**
     * Sets the value of the commentaar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentaar(String value) {
        this.commentaar = value;
    }

}
