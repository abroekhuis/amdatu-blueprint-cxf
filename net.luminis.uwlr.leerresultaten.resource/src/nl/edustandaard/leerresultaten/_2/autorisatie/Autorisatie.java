
package nl.edustandaard.leerresultaten._2.autorisatie;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="autorisatiesleutel" type="{http://www.edustandaard.nl/leerresultaten/2/autorisatie}tpString64"/&gt;
 *         &lt;element name="klantcode" type="{http://www.edustandaard.nl/leerresultaten/2/autorisatie}tpString64"/&gt;
 *         &lt;element name="klantnaam" type="{http://www.edustandaard.nl/leerresultaten/2/autorisatie}tpString64"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "autorisatiesleutel",
    "klantcode",
    "klantnaam"
})
@XmlRootElement(name = "autorisatie")
public class Autorisatie {

    @XmlElement(required = true)
    protected String autorisatiesleutel;
    @XmlElement(required = true)
    protected String klantcode;
    @XmlElement(required = true)
    protected String klantnaam;

    /**
     * Gets the value of the autorisatiesleutel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutorisatiesleutel() {
        return autorisatiesleutel;
    }

    /**
     * Sets the value of the autorisatiesleutel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutorisatiesleutel(String value) {
        this.autorisatiesleutel = value;
    }

    /**
     * Gets the value of the klantcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKlantcode() {
        return klantcode;
    }

    /**
     * Sets the value of the klantcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKlantcode(String value) {
        this.klantcode = value;
    }

    /**
     * Gets the value of the klantnaam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKlantnaam() {
        return klantnaam;
    }

    /**
     * Sets the value of the klantnaam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKlantnaam(String value) {
        this.klantnaam = value;
    }

}
