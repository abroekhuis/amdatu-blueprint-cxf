
package nl.edustandaard.oso_gegevensset._1_2.dossier;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for toetsscoreType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="toetsscoreType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aantalopgaven" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="aantalgoed" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="aantalfout" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="aantalgelezen" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="tijd" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="vaardigheidsscore" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="codevaardigheidsschaal" type="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}nonEmptyStringType" minOccurs="0"/&gt;
 *         &lt;element name="betrouwbaarheidsinterval" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="bivlaag" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;element name="bivhoog" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "toetsscoreType", propOrder = {
    "aantalopgaven",
    "aantalgoed",
    "aantalfout",
    "aantalgelezen",
    "tijd",
    "vaardigheidsscore",
    "codevaardigheidsschaal",
    "betrouwbaarheidsinterval"
})
public class ToetsscoreType {

    protected Integer aantalopgaven;
    protected Integer aantalgoed;
    protected Integer aantalfout;
    protected Integer aantalgelezen;
    protected BigDecimal tijd;
    protected BigDecimal vaardigheidsscore;
    protected String codevaardigheidsschaal;
    protected ToetsscoreType.Betrouwbaarheidsinterval betrouwbaarheidsinterval;

    /**
     * Gets the value of the aantalopgaven property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAantalopgaven() {
        return aantalopgaven;
    }

    /**
     * Sets the value of the aantalopgaven property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAantalopgaven(Integer value) {
        this.aantalopgaven = value;
    }

    /**
     * Gets the value of the aantalgoed property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAantalgoed() {
        return aantalgoed;
    }

    /**
     * Sets the value of the aantalgoed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAantalgoed(Integer value) {
        this.aantalgoed = value;
    }

    /**
     * Gets the value of the aantalfout property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAantalfout() {
        return aantalfout;
    }

    /**
     * Sets the value of the aantalfout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAantalfout(Integer value) {
        this.aantalfout = value;
    }

    /**
     * Gets the value of the aantalgelezen property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAantalgelezen() {
        return aantalgelezen;
    }

    /**
     * Sets the value of the aantalgelezen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAantalgelezen(Integer value) {
        this.aantalgelezen = value;
    }

    /**
     * Gets the value of the tijd property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTijd() {
        return tijd;
    }

    /**
     * Sets the value of the tijd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTijd(BigDecimal value) {
        this.tijd = value;
    }

    /**
     * Gets the value of the vaardigheidsscore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVaardigheidsscore() {
        return vaardigheidsscore;
    }

    /**
     * Sets the value of the vaardigheidsscore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVaardigheidsscore(BigDecimal value) {
        this.vaardigheidsscore = value;
    }

    /**
     * Gets the value of the codevaardigheidsschaal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodevaardigheidsschaal() {
        return codevaardigheidsschaal;
    }

    /**
     * Sets the value of the codevaardigheidsschaal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodevaardigheidsschaal(String value) {
        this.codevaardigheidsschaal = value;
    }

    /**
     * Gets the value of the betrouwbaarheidsinterval property.
     * 
     * @return
     *     possible object is
     *     {@link ToetsscoreType.Betrouwbaarheidsinterval }
     *     
     */
    public ToetsscoreType.Betrouwbaarheidsinterval getBetrouwbaarheidsinterval() {
        return betrouwbaarheidsinterval;
    }

    /**
     * Sets the value of the betrouwbaarheidsinterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link ToetsscoreType.Betrouwbaarheidsinterval }
     *     
     */
    public void setBetrouwbaarheidsinterval(ToetsscoreType.Betrouwbaarheidsinterval value) {
        this.betrouwbaarheidsinterval = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="bivlaag" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;element name="bivhoog" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bivlaag",
        "bivhoog"
    })
    public static class Betrouwbaarheidsinterval {

        @XmlElement(required = true)
        protected BigDecimal bivlaag;
        @XmlElement(required = true)
        protected BigDecimal bivhoog;

        /**
         * Gets the value of the bivlaag property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getBivlaag() {
            return bivlaag;
        }

        /**
         * Sets the value of the bivlaag property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setBivlaag(BigDecimal value) {
            this.bivlaag = value;
        }

        /**
         * Gets the value of the bivhoog property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getBivhoog() {
            return bivhoog;
        }

        /**
         * Sets the value of the bivhoog property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setBivhoog(BigDecimal value) {
            this.bivhoog = value;
        }

    }

}
