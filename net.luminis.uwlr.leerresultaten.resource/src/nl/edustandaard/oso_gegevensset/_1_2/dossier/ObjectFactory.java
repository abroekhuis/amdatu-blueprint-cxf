
package nl.edustandaard.oso_gegevensset._1_2.dossier;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.edustandaard.oso_gegevensset._1_2.dossier package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Toetsscore_QNAME = new QName("http://www.edustandaard.nl/oso_gegevensset/1.2/dossier", "toetsscore");
    private final static QName _Referentiescore_QNAME = new QName("http://www.edustandaard.nl/oso_gegevensset/1.2/dossier", "referentiescore");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.edustandaard.oso_gegevensset._1_2.dossier
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ToetsscoreType }
     * 
     */
    public ToetsscoreType createToetsscoreType() {
        return new ToetsscoreType();
    }

    /**
     * Create an instance of {@link ReferentiescoreType }
     * 
     */
    public ReferentiescoreType createReferentiescoreType() {
        return new ReferentiescoreType();
    }

    /**
     * Create an instance of {@link ToetsscoreType.Betrouwbaarheidsinterval }
     * 
     */
    public ToetsscoreType.Betrouwbaarheidsinterval createToetsscoreTypeBetrouwbaarheidsinterval() {
        return new ToetsscoreType.Betrouwbaarheidsinterval();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ToetsscoreType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.edustandaard.nl/oso_gegevensset/1.2/dossier", name = "toetsscore")
    public JAXBElement<ToetsscoreType> createToetsscore(ToetsscoreType value) {
        return new JAXBElement<ToetsscoreType>(_Toetsscore_QNAME, ToetsscoreType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReferentiescoreType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.edustandaard.nl/oso_gegevensset/1.2/dossier", name = "referentiescore")
    public JAXBElement<ReferentiescoreType> createReferentiescore(ReferentiescoreType value) {
        return new JAXBElement<ReferentiescoreType>(_Referentiescore_QNAME, ReferentiescoreType.class, null, value);
    }

}
