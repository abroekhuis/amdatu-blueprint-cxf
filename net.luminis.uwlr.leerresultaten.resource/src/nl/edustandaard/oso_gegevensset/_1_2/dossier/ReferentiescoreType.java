
package nl.edustandaard.oso_gegevensset._1_2.dossier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for referentiescoreType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="referentiescoreType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codereferentiescore" type="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}nonEmptyStringType"/&gt;
 *         &lt;element name="codevergelijkingsgroep" type="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}nonEmptyStringType"/&gt;
 *         &lt;element name="waarde" type="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}nonEmptyStringType"/&gt;
 *         &lt;element name="kwalificatie" type="{http://www.edustandaard.nl/oso_gegevensset/1.2/dossier}nonEmptyStringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "referentiescoreType", propOrder = {
    "codereferentiescore",
    "codevergelijkingsgroep",
    "waarde",
    "kwalificatie"
})
public class ReferentiescoreType {

    @XmlElement(required = true)
    protected String codereferentiescore;
    @XmlElement(required = true)
    protected String codevergelijkingsgroep;
    @XmlElement(required = true)
    protected String waarde;
    protected String kwalificatie;

    /**
     * Gets the value of the codereferentiescore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodereferentiescore() {
        return codereferentiescore;
    }

    /**
     * Sets the value of the codereferentiescore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodereferentiescore(String value) {
        this.codereferentiescore = value;
    }

    /**
     * Gets the value of the codevergelijkingsgroep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodevergelijkingsgroep() {
        return codevergelijkingsgroep;
    }

    /**
     * Sets the value of the codevergelijkingsgroep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodevergelijkingsgroep(String value) {
        this.codevergelijkingsgroep = value;
    }

    /**
     * Gets the value of the waarde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaarde() {
        return waarde;
    }

    /**
     * Sets the value of the waarde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaarde(String value) {
        this.waarde = value;
    }

    /**
     * Gets the value of the kwalificatie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKwalificatie() {
        return kwalificatie;
    }

    /**
     * Sets the value of the kwalificatie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKwalificatie(String value) {
        this.kwalificatie = value;
    }

}
