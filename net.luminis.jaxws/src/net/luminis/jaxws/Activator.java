package net.luminis.jaxws;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.Servlet;

import org.apache.cxf.bus.extension.Extension;
import org.apache.cxf.bus.extension.ExtensionRegistry;
import org.apache.cxf.bus.osgi.CXFExtensionBundleListener;
import org.apache.cxf.bus.osgi.ManagedWorkQueueList;
import org.apache.cxf.bus.osgi.OSGIBusListener;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

public class Activator extends DependencyActivatorBase {

	private List<Extension> extensions;
	private ManagedWorkQueueList workQueues = new ManagedWorkQueueList();
	private CXFExtensionBundleListener cxfBundleListener;

	@Override
	public void init(BundleContext context, DependencyManager dm) throws Exception {
		cxfBundleListener = new CXFExtensionBundleListener(context.getBundle().getBundleId());
		context.addBundleListener(cxfBundleListener);
		cxfBundleListener.registerExistingBundles(context);

		extensions = new ArrayList<Extension>();
		Extension createOsgiBusListenerExtension = createOsgiBusListenerExtension(context);
		extensions.add(createOsgiBusListenerExtension);
		extensions.add(createManagedWorkQueueListExtension(workQueues));
		ExtensionRegistry.addExtensions(extensions);

		Properties sProps = new Properties();
        sProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/cxf/*");
        dm.add(createComponent()
                .setInterface(Servlet.class.getName(), sProps)
                .setImplementation(JaxWsServlet.class)
                .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy")
                .add(createServiceDependency().setService("(objectClass=*)")
                        .setCallbacks("onAdded", "onRemoved")));
    }

	private Extension createOsgiBusListenerExtension(BundleContext context) {
		Extension busListener = new Extension(OSGIBusListener.class);
		busListener.setArgs(new Object[] { context });
		return busListener;
	}

	private static Extension createManagedWorkQueueListExtension(final ManagedWorkQueueList workQueues) {
		return new Extension(ManagedWorkQueueList.class) {
			public Object getLoadedObject() {
				return workQueues;
			}

			public Extension cloneNoObject() {
				return this;
			}
		};
	}

	@Override
	public void destroy(BundleContext context, DependencyManager arg1) throws Exception {
		context.removeBundleListener(cxfBundleListener);
		cxfBundleListener.shutdown();
		workQueues.shutDown();
		ExtensionRegistry.removeExtensions(extensions);

	}

}
