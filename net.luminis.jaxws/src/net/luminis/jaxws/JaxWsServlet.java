package net.luminis.jaxws;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jws.WebService;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.ws.Endpoint;

import org.apache.cxf.BusFactory;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;
import org.osgi.framework.ServiceReference;

public class JaxWsServlet extends CXFNonSpringServlet {

	private static final long serialVersionUID = 1L;

	Map<ServiceReference<? extends Object>, Endpoint> endpoints = new ConcurrentHashMap<>();

	volatile boolean initDone = false;
	Map<ServiceReference<?>, Object> refs = new ConcurrentHashMap<>();

	public JaxWsServlet() {
		super(null, true);
	}

	@Override
	public void init(ServletConfig sc) throws ServletException {
		super.init(sc);

		// Once the servlet is init'ed, all cached resources can be added.
		synchronized (refs) {
			initDone = true;
			refs.entrySet().forEach(e -> onAdded(e.getKey(), e.getValue()));
			refs.clear();
		}

	}

	public void onAdded(ServiceReference<?> serviceReference, Object service) {
		WebService annotation = service.getClass().getAnnotation(
				WebService.class);

		if (annotation != null) {

			// To be able to publish endpoints, we need to wait for the servlet to be init'ed, so cache the resources, and let them be added by the final "init" call.
			synchronized (refs) {
				if (!initDone) {
					refs.put(serviceReference, service);
					return;
				}

			}

			String value = annotation.name();
			if (value == null || value.trim().length() == 0) {
				value = service.getClass().getSimpleName();
			}

			System.out.println("Create new endpoint for [" + service.getClass().getName() + "] with name [" + value + "]");

			BusFactory.setDefaultBus(getBus());


			ClassLoader tccl = Thread.currentThread().getContextClassLoader();
			try {
				Thread.currentThread().setContextClassLoader(org.apache.cxf.jaxws.spi.ProviderImpl.class.getClassLoader());

				System.out.println("Publish new endpoint to [" + value + "]");

				Endpoint endpoint = Endpoint.publish("/".concat(value), service);
				endpoints.put(serviceReference, endpoint);
			} finally {
			    Thread.currentThread().setContextClassLoader(tccl);

			}
		}
	}

	public void onRemoved(ServiceReference<?> serviceReference, Object service) {
		Endpoint remove = endpoints.remove(serviceReference);
		if (remove != null && remove.isPublished()) {
			remove.stop();
		}
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void dmStop() {
		for (Endpoint endpoint : endpoints.values()) {
			endpoint.stop();
		}
	}
}